DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 AS INTEGER START WITH 1;
CREATE SEQUENCE seq2 AS INTEGER START WITH 1;

CREATE TABLE customer (
       id BIGINT NOT NULL PRIMARY KEY,
       firstname VARCHAR(255),
       lastname VARCHAR(255),
       code VARCHAR(100),
       type VARCHAR(100)
);

CREATE TABLE phone (
      id BIGINT NOT NULL PRIMARY KEY,
      type VARCHAR(255) NOT NULL,
      value VARCHAR(255) NOT NULL,
      customer_id BIGINT NOT NULL,
      FOREIGN KEY (customer_id)
        REFERENCES customer
        ON DELETE RESTRICT
);

CREATE TABLE USERS (
     username VARCHAR(255) NOT NULL PRIMARY KEY,
     password VARCHAR(255) NOT NULL,
     enabled BOOLEAN NOT NULL,
     first_name VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORITIES (
     username VARCHAR(50) NOT NULL,
     authority VARCHAR(50) NOT NULL,
     FOREIGN KEY (username) REFERENCES USERS
       ON DELETE CASCADE
);

CREATE UNIQUE INDEX ix_auth_username ON AUTHORITIES (username, authority);

INSERT INTO USERS (username, password, enabled, first_name) VALUES ('user','$2a$10$//dzzsZBDTJMTgSY0mFyKOByet7DvSOVPc.ABn94eTqam5R8oy4WG', true, 'Jack');
INSERT INTO USERS (username, password, enabled, first_name) VALUES ('admin','$2a$10$gIWZ5HBiFNQiiDEVE2Ox4uamuw/G1pGMeFtU6T5P63C9cUKwfzplq', true, 'Jill');
INSERT INTO AUTHORITIES (username, authority) VALUES ('user', 'ROLE_USER');
INSERT INTO AUTHORITIES (username, authority) VALUES ('admin', 'ROLE_ADMIN');


