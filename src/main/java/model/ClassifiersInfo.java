package model;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class ClassifiersInfo {

    private List<String> phoneTypes;
    private List<String> customerTypes;

    public ClassifiersInfo() {
        phoneTypes = Arrays.asList(
                "phone_type.fixed",
                "phone_type.mobile"
        );
        customerTypes = Arrays.asList(
                "customer_type.private",
                "customer_type.corporate"
        );
    }


}
