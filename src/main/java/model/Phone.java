package model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Phone {
    @Id
    @SequenceGenerator(name = "phone_seq", sequenceName = "seq2", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_seq")
    private Long id;
    private String value;
    private String type;

    public Phone(String number, String type) {
        value = number;
        this.type = type;
    }

}
