package dao;

import model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

    public List<Customer> getCustomers() {
        return em.createQuery("select distinct c from Customer c left join fetch c.phones", Customer.class)
                .getResultList();
    }

    @Transactional
    public void insertCustomer(Customer customer){
        em.persist(customer);
    }

    @Transactional
    public void deleteCustomers() {
        em.createQuery("delete from Phone").executeUpdate();
        em.createQuery("delete from Customer").executeUpdate();
    }

    public Customer getCustomerById(Long id) {
        return em.createQuery("select c from Customer c left join fetch c.phones where c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional
    public void deleteCustomerById(long id) {
        em.createQuery("delete from Customer c where c.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    public List<Customer> search(String key) {
        return em.createQuery(
                "select distinct c from Customer c left join fetch c.phones where " +
                        "lower(c.firstName) LIKE :key OR " +
                        "lower(c.lastName) LIKE :key OR " +
                        "lower(c.code) LIKE :key", Customer.class)
                .setParameter("key", '%' + key + '%')
                .getResultList();
    }

}
