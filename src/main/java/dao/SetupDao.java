package dao;

import util.DbUtil;
import util.FileUtil;
import util.PropertyLoader;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SetupDao {

    private static String DB_URL = PropertyLoader.getProperty("javax.persistence.jdbc.url");

    public void createSchema() {
        String statements = FileUtil.readFileFromClasspath("schema.sql");

        try {
            DbUtil.insertFromString(DriverManager.getConnection(DB_URL), statements);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
