package user;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public User getUserByUserName(String userName) {
        return em.createQuery("select username, first_name from Users u where u.username = :userName OR u.first_name = :userName", User.class)
                .setParameter("userName", userName)
                .getSingleResult();
    }
}
