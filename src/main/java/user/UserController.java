package user;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import user.User;
import user.UserDao;

import javax.annotation.Resource;

@RestController
public class UserController {

    @Resource
    UserDao dao;

    @GetMapping("users/{userName}")
    @PreAuthorize("#username == authentication.name || hasRole('ADMIN')")
    public User getUserByName(@PathVariable String userName) {

        return dao.getUserByUserName(userName);

    }
}
