package controller;

import dao.CustomerDao;
import model.ClassifiersInfo;
import model.Customer;
import org.springframework.web.bind.annotation.*;
import user.User;
import user.UserDao;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDao dao;

    @GetMapping("customers")
    public List<Customer> getCustomers(){
        return dao.getCustomers();
    }

    @GetMapping("customers/{id}")
    public Customer getById(@PathVariable Long id) {
        return dao.getCustomerById(id);
    }

    @PostMapping("customers")
    public void saveCustomer(@RequestBody @Valid Customer customer){
        dao.insertCustomer(customer);
    }

    @DeleteMapping("customers/{id}")
    public void deleteCustomerById(@PathVariable Long id){
        dao.deleteCustomerById(id);
    }

    @DeleteMapping("customers")
    public void deleteAllCustomers(){
        dao.deleteCustomers();
    }

    @GetMapping("classifiers")
    public ClassifiersInfo getClassifiers(){
        return new ClassifiersInfo();
    }

    @GetMapping("customers/search")
    public List<Customer> search(@RequestParam(defaultValue = "") String key){
        return dao.search(key);
    }


}
